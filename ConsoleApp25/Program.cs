﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ConsoleApp25
{
    class Program
    {
        static void Main(string[] args)
        {
            string inimesedFile = @"..\..\Inimesed.txt";
            string sugulasedFile = @"..\..\Sugulased.txt";
            string sugulasedFileTest = @"..\..\Sugulased.txt";
            // see teine on fail kontrollimiseks, et ega salvestamisel miski näsus ei läinud

            // failist lugemisel mõlemad failid mällu stringimassiiviks
            string[] iString = File.ReadAllLines(inimesedFile).Where(x => x.Substring(0,2) != "//").ToArray();
            string[] sString = File.ReadAllLines(sugulasedFile).Where(x => x.Substring(0, 2) != "//").ToArray();

            // käin läbi esimese massiivi
            // kood, nimi
            // ja pistan kõik inimesed mällu
            // new operaator tekitab nad ja paneb inimeste dictionary sisse
            foreach (var rida in iString)
            {
                string[] rr = rida.Split(',').Select(x => x.Trim()).ToArray();
                new Inimene(int.Parse(rr[0])) { Nimi = rr[1] };
            }
            
            // käin läbi teise massiivi
            // kood, kood, kaasa
            // kood, kood, lapsed, kood, ...
            foreach (var rida in sString)
            {
                string[] rr = rida.Split(',').Select(x => x.Trim()).ToArray();
                int v = int.Parse(rr[0]); // kaks muutujat kahe koodi salvestamiseks
                int p = int.Parse(rr[1]);
                switch(rr[2])    // kolmas element massiivis ütleb, kas kaasa või lapsed
                {
                    case "kaasa":
                        Inimene.Inimesed[v].Kaasa = Inimene.Inimesed[p];
                        // vaatame korraks veel Inimene klassi propertyt Kaasa
                        break;
                    case "lapsed":
                        // siin võtame massiivist rr alates 4. elemendist
                        foreach (var x in rr.Skip(3).Select( x => int.Parse(x)))
                        {
                            // igale numbrile vastava inimese lapsed paneme paika
                            // nb, kui on teada vaid üks vanem, on teise vanema kohal 0
                            Inimene.Inimesed[x].SetVanemad(Inimene.Inimesed[v], p == 0 ? null : Inimene.Inimesed[p]);
                        }
                        break;
                }
            }

            
            // see trükib loetud inimesed välja (lapsi ei trüki)
            foreach (var i in Inimene.Inimesed.Values)
            {
                Console.WriteLine(i);
                //Console.WriteLine($"{i.Nimi} ema: {i.Ema?.Nimi} isa: {i.Isa?.Nimi} isa-kaasa: {i.Isa?.Kaasa?.Nimi}");
            }

            // algab dialoog
            string vastus = "";
            do {
                // küsimus - mis teeme ja neli vastusevarianti
                Console.Write("misteeme (list, uus, laps, kaasa): ");
                switch(vastus = Console.ReadLine().ToLower())
                {
                    case "list":    // list trükib praeguse seisu
                        foreach (var i in Inimene.Inimesed.Values)
                        {
                            Console.WriteLine(i);
                        }

                        break;
                    case "uus":     // teeb uue inimese
                        Console.Write("nimi: ");
                        Inimene uus = new Inimene() { Nimi = Console.ReadLine() };
                        Console.WriteLine($"lisasin numbriga  {uus.Kood} nimega {uus.Nimi}") ;
                        break;

                    case "laps":    // määrab, kes on kelle laps (ema ja laps)
                                    // NB! me ei tee hetkel isal-emal vahet
                                    // kui vanemal on kaasa, saavad lapse mõlemad
                                    // kui vanemal ei ole kaasat, jääb ilma teise vanemata
                        Console.Write("Kes on kelle laps (ema, laps):");
                        int[] rr = Console.ReadLine().Split(',').Select(x => int.Parse(x.Trim())).ToArray();
                        Inimene.Inimesed[rr[1]].SetVanemad(Inimene.Inimesed[rr[0]], Inimene.Inimesed[rr[0]].Kaasa);
                        
                        break;
                    case "kaasa":   // see määrab, kes on kelle kaasa
                                    // NB! siin puudub praegu normaalne lahutusprotsess, et kui 
                                    // juba on kaasa, siis seda ei eemaldata
                                    // koos vaatame, mis teha, et lahtutus käigult anda
                        Console.Write("Kes on kelle kaasa (naine, mees):");
                        rr = Console.ReadLine().Split(',').Select(x => int.Parse(x.Trim())).ToArray();

                        Inimene.Inimesed[rr[0]].Kaasa = Inimene.Inimesed[rr[1]];

                        break;
                }
            } while (vastus != "");

            // siit hakkab salvestuse osa ja selle seletamise jätan hilisemaks
            using (StreamWriter sw = new StreamWriter(inimesedFile))
            {

                // esimene osa on lihtne, salvestame inimesed
                foreach (var i in Inimene.Inimesed.Values)
                {
                    sw.WriteLine($"{i.Kood}, {i.Nimi}");
                }
            }

            using (StreamWriter sw = new StreamWriter(sugulasedFileTest))
            {
                // teine osa on lihtne, salvestame, kes on kelle kaasa
                //sw.WriteLine("// siit algavad kaasad");
                foreach (var i in Inimene.Inimesed.Values
                    .Where(x => x.Kaasa != null)
                    .Where( x => x.Kood > x.Kaasa.Kood)
                  )
                {
                    sw.WriteLine($"{i.Kood},{i.Kaasa.Kood}, kaasa");
                }

                // siit läheb keerulisemaks
                foreach (var i in Inimene.Inimesed.Values.Where(x => x.Lapsed.Count > 0))
                {
                  //  sw.WriteLine("// siin peaks olema ühe vanemaga lapsed");
                    // kuna me emal-isal vahet ei tee, siis ühe vanemaga laste
                    // ema on see üks vanem ja isa on null
                    // kõigepealt siis ühe vanemaga lapsed
                    if (i.Lapsed.Where(x => x.Isa == null).Count() > 0)
                        sw.WriteLine($"{i.Kood},0,lapsed" +
                        string.Concat(
                        i.Lapsed.Where(x => x.Isa == null).Select(x => $",{x.Kood}")));

                    // see võrdlus on vajalik, et lapsed ei läheks topelt
                    // teiseks siis nn abielulised lapsed
                    if (i.Kood > i.KaasaKood)
                    {
                        if (i.Lapsed
                        .Where(x => x.Isa != null)
                        .Where(x => x.Isa.Kood == x.Ema.KaasaKood)
                        .Count() > 0)
                        {
                    //        sw.WriteLine("// siit algavad abielulised lapsed");
                            sw.WriteLine($"{i.Kood},{i.KaasaKood},lapsed" +
                                    string.Concat(
                                        i.Lapsed
                                            .Where(x => x.Isa != null)
                                            .Where(x => x.Isa.Kood == x.Ema.KaasaKood)
                                            .Select(x => $",{x.Kood}")));

                        }
                    }


                    // ja siis ühe vanemaga lapsed
                    //foreach (var l in i.Lapsed
                    //    .Where(x => x.Isa == null))
                    //    sw.WriteLine($"{l.Ema.Kood},0,lapsed,{l.Kood}");


                }
                // viimaks kõik ülejäänud 2 vanemaga lapsed
                // kelle isa != null (need on esimeses nimekirjas)
                // kelle isa-ema ei ole abielus (abielulised on teises nimekirjas)
                // kelle isa kood < ema koodist (et vältida duplikaate)
                //sw.WriteLine("// siit algavad kahe vanemaga abielueelsed lapsed");
                foreach (var l in Inimene.Inimesed.Values
                    .Where(x => x.Ema != null)
                    .Where(x => x.Isa != null)
                    .Where(x => x.Isa.Kood != x.Ema.KaasaKood)
                    .Where(x => x.Isa.Kood < x.Ema.Kood))
                    sw.WriteLine($"{l.Ema.Kood},{l.Isa.Kood},lapsed,{l.Kood}");

            }
        }
    }


    // Alustuseks teeme klassi Inimene
    class Inimene
    {
        // dictionary, kus hoiame kõiki inimesi
        public static Dictionary<int, Inimene> Inimesed = new Dictionary<int, Inimene>();

        
        public int Kood; // inimese unikaalne kood
        public string Nimi; // inimese nimi
        private Inimene _Kaasa; // sisemine väärtus Kaasa (vt propertit)
        public Inimene Ema;    // vanemad
        public Inimene Isa;
        public List<Inimene> Lapsed = new List<Inimene>();

        // kaks konstruktorit - üks juhuks kui vaja luua teada koodiga inimene
        // näiteks failist lugemis käigus
        public Inimene(int kood)
        {
            this.Kood = kood;
            Inimesed.Add(kood, this);
        }

        // teine konstruktor uue inimese tegemiseks 
        // kood arvutatakse automaatselt
        public Inimene() : this( Inimesed.Keys.Max() + 1 )
        {
        }

        // property Kaasa
        // lugemisel sisemine _Kaasa
        // kirjutamisel mõlemad kaasad vastastikku
        public Inimene Kaasa
        {
            get => _Kaasa;
            set
            {
                if (this.Kaasa != null) this.Kaasa._Kaasa = null;
                if (value.Kaasa != null) value.Kaasa._Kaasa = null;
                this._Kaasa = value;
                value._Kaasa = this;
            }
        }

        public int KaasaKood { get => _Kaasa == null ? 0 : _Kaasa.Kood; }

        public void SetVanemad(Inimene ema, Inimene isa)
        {
            this.Ema = ema;
            this.Isa = isa;
            ema.Lapsed.Add(this);
            isa?.Lapsed.Add(this);
        }

        public override string ToString()
        {
            return $"{Kood} {Nimi} isa: {Isa?.Nimi} ema: {Ema?.Nimi} lapsi: {Lapsed.Count} kaasa: {Kaasa?.Nimi}";
        }

    }
}
